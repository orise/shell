#!/bin/bash 

# To create module file based on package installed path.
# By Shun Xu, 16 Nov 2021

function echo_color() {
  #color $*
  case $1 in
  red)
   shift 1;
   printf "\033[31m$*\033[0m\n"
   ;;
  green)
   shift 1;
   printf "\033[32m$*\033[0m\n"
  ;;
  yellow)
   shift 1;
   printf "\033[33m$*\033[0m\n"
  ;;
  blue)
   shift 1;
   printf "\033[34m$*\033[0m\n"
  ;;
  *)
    printf "$*\n"
  ;;
  esac
}

#./create_module_file package-installed-path package-version modufile-path

create_module_file(){
  if [ $# -ne 3 ]; then
    echo_color red "input: package-installed-path package-version modufile-path"
    return 1
  fi

  local PKG_INSTALLED_PATH=$1
  local PKG_NAME_VER=$2
  local MODFILE=$3

  if [ ! -d $PKG_INSTALLED_PATH ]; then
    echo_color red "$PKG_INSTALLED_PATH not installed to create module file" 1>&2
    return 1
  fi
  local res=$(cd $PKG_INSTALLED_PATH ;find . -name bin -or -name sbin -or -name lib -or -name lib64 -or -name share)
  if [ -z "$res" ] ; then
    echo_color red "$PKG_INSTALLED_PATH no need to create module file" 1>&2
    return 1
  fi

  local MODDIR=$(dirname $MODFILE)
  if [ ! -d $MODDIR ]; then
    mkdir -p $MODDIR 
    if [ ! -d $MODDIR ]; then
       echo_color red "Module directory '$MODDIR' cann't be created" 1>&2
       return 1
    fi
  fi

  local PKGNAME
  local PKGVER
  if [[ "$PKG_NAME_VER" == *-* ]] ; then
    PKGNAME=${PKG_NAME_VER%-*}
    PKGVER=${PKG_NAME_VER#*-}
  else
    PKGNAME=${PKG_NAME_VER}
    PKGVER="1.0.0"
  fi

  local OS_TYPE=$(uname -s)
  local pkgname_upper=$(echo ${PKGNAME} | awk '{print toupper($0)}' )
  cat >$MODFILE <<EOF
#%Module

set name "$PKGNAME"
set ver "$PKGVER"
set kern [ exec uname -s ]
module-whatis "Name        : \$name"
module-whatis "Version     : \$ver"
module-whatis "Description : \${name}-\${ver} for \$kern"
set base_dir  $PKG_INSTALLED_PATH
setenv ${pkgname_upper}_ROOT  \$base_dir
EOF
  
  for x in sbin bin libexec; do
    if [ -d $PKG_INSTALLED_PATH/$x ] ; then
      echo "prepend-path  PATH   \$base_dir/$x" >>$MODFILE
    fi 
  done
  
  for x in lib lib64 ; do
    if [ -d $PKG_INSTALLED_PATH/$x ] ; then
      echo "prepend-path  LD_LIBRARY_PATH \$base_dir/$x"  >>$MODFILE
      if [ "$OS_TYPE" = "Darwin" ] ; then
        echo "prepend-path  DYLD_LIBRARY_PATH \$base_dir/$x"  >>$MODFILE
      fi
    fi
  done

  for x in $(cd $PKG_INSTALLED_PATH ; find . -maxdepth 2 -name man | cut -b3-) ; do
    echo "prepend-path  MANPATH   \$base_dir/$x"  >>$MODFILE
  done

  for x in $(cd $PKG_INSTALLED_PATH ; find . -maxdepth 2 -name info | cut -b3-) ; do
    echo "prepend-path  INFOPATH   \$base_dir/$x"  >>$MODFILE
  done

  for x in $(cd $PKG_INSTALLED_PATH ; find . -maxdepth 2 -type d -name pkgconfig | cut -b3-) ; do
    echo "prepend-path  PKG_CONFIG_PATH   \$base_dir/$x"  >>$MODFILE
  done

  local CMAKE_PATHS
  for x in $(cd $PKG_INSTALLED_PATH ; find . -maxdepth 5 -type f -name Find*.cmake -or -name *Config.cmake -or -name *-config.cmake | cut -b3-) ; do
    if [ -z "$CMAKE_PATHS" ]; then
      CMAKE_PATHS=$(dirname $x)
    else
      CMAKE_PATHS=${CMAKE_PATHS}:$(dirname $x)
    fi
  done
  if [ -n "$CMAKE_PATHS" ] ; then
     awk -F: '{
       for (i = 1; i <= NF; ++i)
          if (unique[$i] != 1){
             printf("prepend-path  CMAKE_MODULE_PATH  $base_dir/%s\n",$i)
             unique[$i] = 1
          }
     }' <<< $CMAKE_PATHS  >>$MODFILE
  fi
  
  return 0
}


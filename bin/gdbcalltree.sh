#!/bin/bash
if [ -z "$GDB_ARGS" ] ; then
   echo "GDB_ARGS env required"
   exit 1
fi
gdbargs=$GDB_ARGS
breakpoints=$*
echo $breakpoints
#"compute_pressure_grem.cpp:73 fix_grem.cpp:271"
gdblog=gdb_log.txt

if [ -f $gdblog ]; then rm -f $gdblog ; fi

cat << EOF > gdb.cmd
# Turn off output to the screen
set logging redirect on

set logging file $gdblog
set logging on

# Backtraces will stop when they encounter the user entry point. This is the default. 
set backtrace past-main off

# Limit the backtrace to n levels. A value of zero means unlimited. 
set backtrace limit 0

# Prevent gdb from stopping after a screenful of output
# set height 0 ; OR
set pagination off

# Break main and run
start
EOF

for bp in ${breakpoints}; do
    cat << EOF >> gdb.cmd
break $bp
command
backtrace
continue
end

EOF
done

#shell rm -f  $gdblog

cat << EOF >> gdb.cmd
continue
quit
EOF

#ulimit -c unlimited

#-quiet 
[ -e $gdblog ] && rm  $gdblog
gdb -command=gdb.cmd --args $gdbargs

if [ -e "./gdbcalltree.py" ] ;then
  gct="./gdbcalltree.py"
elif which gdbcalltree.py ; then
  gct="gdbcalltree.py"
fi

if [ -n "$gct" ]; then
  #conda activate py3mol
  export PATH=${PATH}:/home/alwin/miniconda3/envs/py3mol/bin
  $gct $gdblog | dot -o ${gdblog/.txt/.png} -Tpng
fi

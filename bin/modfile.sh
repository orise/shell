#/bin/bash
#chmod all files in normal mod
if [ $# -lt 1 ] ; then
  echo "$0 <file|dir>"
  exit 1
fi
for f in `find $*` ; do
  ftype=$(file $f|awk '{if (NF<3) {print $2} else {print $2,$3} }')
  nmd=644
  case $ftype in
        ELF*|*shell|"libtool library"|directory)
          nmd=755
        ;;
        symbolic*)
          nmd=777
        ;;
        *)
          nmd=644
        ;;
  esac
  echo $ftype,$nmd,$f
  chmod $nmd $f
done

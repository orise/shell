#!/usr/bin/env python3
#
# Copyright 2019-2020 Shun Xu
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Dump call tree or generate a dot graph from the output of gdb backtrace output."""

__author__ = "Shun Xu"
__date__ = "22 Apr, 2019"

import sys
import math
import os.path
import re
import textwrap
import optparse
import fnmatch

# Python 2.x/3.x compatibility
if sys.version_info[0] >= 3:
    PYTHON_3 = True

    def compat_iteritems(x):
        return x.items()  # No iteritems() in Python 3

    def compat_itervalues(x):
        return x.values()  # No itervalues() in Python 3

    def compat_keys(x):
        return list(x.keys())  # keys() is a generator in Python 3

    basestring = str  # No class basestring in Python 3
    unichr = chr  # No unichr in Python 3
    xrange = range  # No xrange in Python 3
else:
    PYTHON_3 = False

    def compat_iteritems(x):
        return x.iteritems()

    def compat_itervalues(x):
        return x.itervalues()

    def compat_keys(x):
        return x.keys()


########################################################################
# Model

MULTIPLICATION_SIGN = unichr(0xd7)


def times(x):
    return "%u%s" % (x, MULTIPLICATION_SIGN)


def percentage(p):
    return "%.02f%%" % (p * 100.0, )


def add(a, b):
    return a + b


def fail(a, b):
    assert False


tol = 2**-23


def ratio(numerator, denominator):
    try:
        ratio = float(numerator) / float(denominator)
    except ZeroDivisionError:
        # 0/0 is undefined, but 1.0 yields more useful results
        return 1.0
    if ratio < 0.0:
        if ratio < -tol:
            sys.stderr.write(
                'warning: negative ratio (%s/%s)\n' % (numerator, denominator))
        return 0.0
    if ratio > 1.0:
        if ratio > 1.0 + tol:
            sys.stderr.write('warning: ratio greater than one (%s/%s)\n' %
                             (numerator, denominator))
        return 1.0
    return ratio


class UndefinedEvent(Exception):
    """Raised when attempting to get an event which is undefined."""

    def __init__(self, event):
        Exception.__init__(self)
        self.event = event

    def __str__(self):
        return 'unspecified event %s' % self.event.name


class Event(object):
    """Describe a kind of event, and its basic operations."""

    def __init__(self, name, null, aggregator, formatter=str):
        self.name = name
        self._null = null
        self._aggregator = aggregator
        self._formatter = formatter

    def __eq__(self, other):
        return self is other

    def __hash__(self):
        return id(self)

    def null(self):
        return self._null

    def aggregate(self, val1, val2):
        """Aggregate two event values."""
        assert val1 is not None
        assert val2 is not None
        return self._aggregator(val1, val2)

    def format(self, val):
        """Format an event value."""
        assert val is not None
        return self._formatter(val)


CALLS = Event("Calls", 0, add, times)
FILE_LINE = Event("Fileline", "", fail)

SAMPLES = Event("Samples", 0, add, times)
SAMPLES2 = Event("Samples", 0, add, times)

# Count of samples where a given function was either executing or on the stack.
# This is used to calculate the total time ratio according to the
# straightforward method described in Mike Dunlavey's answer to
# stackoverflow.com/questions/1777556/alternatives-to-gprof, item 4 (the myth
# "that recursion is a tricky confusing issue"), last edited 2012-08-30: it's
# just the ratio of TOTAL_SAMPLES over the number of samples in the profile.
#
# Used only when totalMethod == callstacks
TOTAL_SAMPLES = Event("Samples", 0, add, times)
TIME = Event("Time", 0.0, add, lambda x: '(' + str(x) + ')')
TIME_RATIO = Event("Time ratio", 0.0, add, lambda x: '(' + percentage(x) + ')')
TOTAL_TIME = Event("Total time", 0.0, fail)
TOTAL_TIME_RATIO = Event("Total time ratio", 0.0, fail, percentage)


class Object(object):
    """Base class for all objects in profile which can store events."""

    def __init__(self, events=None):
        if events is None:
            self.events = {}
        else:
            self.events = events

    def __hash__(self):
        return id(self)

    def __eq__(self, other):
        return self is other

    def __lt__(self, other):
        return id(self) < id(other)

    def __contains__(self, event):
        return event in self.events

    def __getitem__(self, event):
        try:
            return self.events[event]
        except KeyError:
            raise UndefinedEvent(event)

    def __setitem__(self, event, value):
        if value is None:
            if event in self.events:
                del self.events[event]
        else:
            self.events[event] = value


class Call(Object):
    """A call between functions.
    
    There should be at most one call object for every pair of functions.
    """

    def __init__(self, callee_id):
        Object.__init__(self)
        self.callee_id = callee_id
        self.ratio = None
        self.weight = None


class Function(Object):
    """A function."""

    def __init__(self, id, name):
        Object.__init__(self)
        self.id = id
        self.name = name
        self.module = None
        self.process = None
        self.calls = {}
        self.called = None
        self.weight = None
        self.cycle = None
        self.args = None
        self.filename = None
        self.fileline = 0

    def add_call(self, call):
        if call.callee_id in self.calls:
            sys.stderr.write(
                'warning: overwriting call from function %s to %s\n' % (str(
                    self.id), str(call.callee_id)))
        self.calls[call.callee_id] = call

    def get_call(self, callee_id):
        if not callee_id in self.calls:
            call = Call(callee_id)
            call[SAMPLES] = 0
            call[SAMPLES2] = 0
            call[CALLS] = 0
            self.calls[callee_id] = call
        return self.calls[callee_id]

    _parenthesis_re = re.compile(r'\([^()]*\)')
    _angles_re = re.compile(r'<[^<>]*>')
    _const_re = re.compile(r'\s+const$')

    def stripped_name(self):
        """Remove extraneous information from C++ demangled function names."""

        name = self.name

        # Strip function parameters from name by recursively removing paired parenthesis
        while True:
            name, n = self._parenthesis_re.subn('', name)
            if not n:
                break

        # Strip const qualifier
        name = self._const_re.sub('', name)

        # Strip template parameters from name by recursively removing paired angles
        while True:
            name, n = self._angles_re.subn('', name)
            if not n:
                break

        return name

    # TODO: write utility functions

    def __repr__(self):
        return self.name


class Profile(Object):
    """The whole profile."""

    def __init__(self):
        Object.__init__(self)
        self.functions = {}
        self.cycles = []

    def validate(self):
        """Validate the edges."""

        for function in compat_itervalues(self.functions):
            for callee_id in compat_keys(function.calls):
                assert function.calls[callee_id].callee_id == callee_id
                if callee_id not in self.functions:
                    sys.stderr.write(
                        'warning: call to undefined function %s from function %s\n'
                        % (str(callee_id), function.name))
                    del function.calls[callee_id]

    def add_function(self, function):
        if function.id in self.functions:
            sys.stderr.write('warning: overwriting function %s (id %s)\n' %
                             (function.name, str(function.id)))
        self.functions[function.id] = function

    def getFunctionIds(self, funcName):
        function_names = {v.name: k for (k, v) in self.functions.items()}
        return [
            function_names[name]
            for name in fnmatch.filter(function_names.keys(), funcName)
        ]

    def getFunctionId(self, funcName):
        for f in self.functions:
            if self.functions[f].name == funcName:
                return f
        return False

    def dump(self):
        for function in compat_itervalues(self.functions):
            sys.stderr.write('Function %s:\n' % (function.name, ))
            self._dump_events(function.events)
            for call in compat_itervalues(function.calls):
                callee = self.functions[call.callee_id]
                sys.stderr.write('  Call %s:\n' % (callee.name, ))
                self._dump_events(call.events)
        for cycle in self.cycles:
            sys.stderr.write('Cycle:\n')
            self._dump_events(cycle.events)
            for function in cycle.functions:
                sys.stderr.write('  Function %s\n' % (function.name, ))

    def _dump_events(self, events):
        for event, value in compat_iteritems(events):
            sys.stderr.write(
                '    %s: %s\n' % (event.name, event.format(value)))

    def prune_root(self, roots, depth=-1):
        visited = set()
        frontier = set([(root_node, depth) for root_node in roots])
        while len(frontier) > 0:
            node, node_depth = frontier.pop()
            visited.add(node)
            if node_depth == 0:
                continue
            f = self.functions[node]
            newNodes = set(f.calls.keys()) - visited
            frontier = frontier.union({(new_node, node_depth - 1)
                                       for new_node in newNodes})
        subtreeFunctions = {}
        for n in visited:
            f = self.functions[n]
            newCalls = {}
            for c in f.calls.keys():
                if c in visited:
                    newCalls[c] = f.calls[c]
            f.calls = newCalls
            subtreeFunctions[n] = f
        self.functions = subtreeFunctions


########################################################################
# Parsers


class Struct:
    """Masquerade a dictionary with a structure-like behavior."""

    def __init__(self, attrs=None):
        if attrs is None:
            attrs = {}
        self.__dict__['_attrs'] = attrs

    def __getattr__(self, name):
        try:
            return self._attrs[name]
        except KeyError:
            raise AttributeError(name)

    def __setattr__(self, name, value):
        self._attrs[name] = value

    def __str__(self):
        return str(self._attrs)

    def __repr__(self):
        return repr(self._attrs)


class ParseError(Exception):
    """Raised when parsing to signal mismatches."""

    def __init__(self, msg, line):
        Exception.__init__(self)
        self.msg = msg
        # TODO: store more source line information
        self.line = line

    def __str__(self):
        return '%s: %r' % (self.msg, self.line)


class Parser:
    """Parser interface."""

    stdinInput = True
    multipleInput = False

    def __init__(self):
        pass

    def parse(self):
        raise NotImplementedError


class GDBBtParser(Parser):
    """Parser for GNU GDB backtrace output.

    See also:
    - GDB Backtraces
      https://ftp.gnu.org/old-gnu/Manuals/gdb/html_node/gdb_42.html
      https://sourceware.org/gdb/onlinedocs/gdb/Backtrace.html
    """

    def __init__(self, fp):
        Parser.__init__(self)
        self.fp = fp
        self.functions = {}
        self.cycles = {}

    _int_re = re.compile(r'^\d+$')
    _float_re = re.compile(r'^\d+\.\d+$')

    _cg_header_re = re.compile(r'hit Breakpoint \d+,')
    _cg_fun1_re = re.compile(
        r'^#(\d+)\s+(\w+ in\s+)?([\w_:?]+)\s\(([^)]*)\)\s+at\s+([^:]+):(\d+)$')
    _cg_fun2_re = re.compile(r'^#(\d+)\s+(\w+ in\s+)?([\w_:?]+)\s\(([^)]*)\)(\s+from\s+.+)?$')

    def parse_cg_entry(self, line):
        attrs = {}
        if line.find(" at ") > -1:
            g = self._cg_fun1_re.match(line)
            if g:
                attrs["id"] = g[1]
                attrs["name"] = g[3]
                attrs["args"] = g[4]
                attrs["file_name"] = g[5]
                attrs["file_line"] = g[6]
        else:
            g = self._cg_fun2_re.match(line)
            if g:
                attrs["id"] = g[1]
                attrs["name"] = g[3]
                attrs["args"] = g[4]
                attrs["file_name"] = g[5]
                attrs["file_line"] = ""

        if attrs:
            attrs["children"] = {}
            attrs["called"] = 0
            return Struct(attrs)
        else:
            return None

    def get_key(self, func):
        #return func.name+"_"+func.args
        return func.name

    def parse_cg(self):
        """Parse the call graph."""
        # process call graph entries
        line = self.fp.readline()
        while line != "":  # form feed
            # skip call graph header
            while not self._cg_header_re.search(line):
                line = self.fp.readline()
                if line == "":
                    break
                continue
            line = self.fp.readline()
            while re.search(r'^[^#]', line):
                line = self.fp.readline()
                if line == "":
                    break
            if line == "":
                break
            local_cg = []
            while re.match(r'^#\d+', line):
                func_strut = self.parse_cg_entry(line)
                if func_strut:
                    local_cg.append(func_strut)
                line = self.fp.readline()
                if line == "":
                    break
            if len(local_cg) > 0:
                for func in local_cg:
                    key = self.get_key(func)
                    if key not in self.functions:
                        self.functions[key] = func
                i = 0
                for func in local_cg:
                    key = self.get_key(func)
                    if i == 0:
                        self.functions[key].called += 1
                    if i + 1 < len(local_cg):
                        #find parent
                        key_par = self.get_key(local_cg[i + 1])
                        self.functions[key_par].called += 1
                        if key not in self.functions[key_par].children:
                            if local_cg[i + 1].file_line != "" :
                                self.functions[key_par].children[key] = [
                                    1, os.path.basename(local_cg[i + 1].file_name) +
                                    ":" + local_cg[i + 1].file_line ]
                            elif local_cg[i + 1].file_name:
                                self.functions[key_par].children[key] = [
                                    1, os.path.basename(local_cg[i + 1].file_name) ]
                            else:
                                self.functions[key_par].children[key] = [1, ""]
                        else:
                            self.functions[key_par].children[key][0] += 1
                    i += 1

    def dump(self, fh):
        for key in compat_keys(self.functions):
            fh.write(key + "\n")
            for ch in self.functions[key].children:
                fh.write("\t" + ch + "\n")

    def parse(self):
        self.parse_cg()
        self.fp.close()

        profile = Profile()
        profile[TIME] = 0.0

        weights = []
        for function in compat_itervalues(self.functions):
            try:
                weights.append(function.called)
            except UndefinedEvent:
                pass
        max_ratio = max(weights or [1])

        funIDs = compat_keys(self.functions)
        for entry in compat_itervalues(self.functions):
            # populate the function
            key = self.get_key(entry)
            fid = funIDs.index(key)
            function = Function(fid, entry.name)
            function[TIME] = 0
            function.weight = ratio(entry.called, max_ratio)
            function.called = entry.called

            # populate the function calls
            for child in entry.children:
                fid = funIDs.index(child)
                call = Call(fid)
                ncall = entry.children[child][0]
                call.weight = ratio(ncall, entry.called) * 0.9

                #assert child.called is not None
                call[CALLS] = ncall
                call[FILE_LINE] = entry.children[child][1]

                function.add_call(call)

            profile.add_function(function)

        # Compute derived events
        profile.validate()

        return profile


formats = {"gdbbt": GDBBtParser}

########################################################################
# Output


class Theme:
    def __init__(self,
                 bgcolor=(0.0, 0.0, 1.0),
                 mincolor=(0.0, 0.0, 0.0),
                 maxcolor=(0.0, 0.0, 1.0),
                 fontname="Courier",
                 fontcolor="white",
                 nodestyle="filled",
                 minfontsize=10.0,
                 maxfontsize=10.0,
                 minpenwidth=0.5,
                 maxpenwidth=4.0,
                 gamma=2.2,
                 skew=1.0):
        self.bgcolor = bgcolor
        self.mincolor = mincolor
        self.maxcolor = maxcolor
        self.fontname = fontname
        self.fontcolor = fontcolor
        self.nodestyle = nodestyle
        self.minfontsize = minfontsize
        self.maxfontsize = maxfontsize
        self.minpenwidth = minpenwidth
        self.maxpenwidth = maxpenwidth
        self.gamma = gamma
        self.skew = skew

    def graph_bgcolor(self):
        return self.hsl_to_rgb(*self.bgcolor)

    def graph_fontname(self):
        return self.fontname

    def graph_fontcolor(self):
        return self.fontcolor

    def graph_fontsize(self):
        return self.minfontsize

    def node_bgcolor(self, weight):
        return self.color(weight)

    def node_fgcolor(self, weight):
        if self.nodestyle == "filled":
            return self.graph_bgcolor()
        else:
            return self.color(weight)

    def node_fontsize(self, weight):
        return self.fontsize(weight)

    def node_style(self):
        return self.nodestyle

    def edge_color(self, weight):
        return self.color(weight)

    def edge_fontsize(self, weight):
        return self.fontsize(weight)

    def edge_penwidth(self, weight):
        return max(weight * self.maxpenwidth, self.minpenwidth)

    def edge_arrowsize(self, weight):
        return 0.5 * math.sqrt(self.edge_penwidth(weight))

    def fontsize(self, weight):
        return max(weight**2 * self.maxfontsize, self.minfontsize)

    def color(self, weight):
        weight = min(max(weight, 0.0), 1.0)

        hmin, smin, lmin = self.mincolor
        hmax, smax, lmax = self.maxcolor

        if self.skew < 0:
            raise ValueError("Skew must be greater than 0")
        elif self.skew == 1.0:
            h = hmin + weight * (hmax - hmin)
            s = smin + weight * (smax - smin)
            l = lmin + weight * (lmax - lmin)
        else:
            base = self.skew
            h = hmin + ((hmax - hmin) * (-1.0 + (base**weight)) / (base - 1.0))
            s = smin + ((smax - smin) * (-1.0 + (base**weight)) / (base - 1.0))
            l = lmin + ((lmax - lmin) * (-1.0 + (base**weight)) / (base - 1.0))

        return self.hsl_to_rgb(h, s, l)

    def hsl_to_rgb(self, h, s, l):
        """Convert a color from HSL color-model to RGB.

        See also:
        - http://www.w3.org/TR/css3-color/#hsl-color
        """

        h = h % 1.0
        s = min(max(s, 0.0), 1.0)
        l = min(max(l, 0.0), 1.0)

        if l <= 0.5:
            m2 = l * (s + 1.0)
        else:
            m2 = l + s - l * s
        m1 = l * 2.0 - m2
        r = self._hue_to_rgb(m1, m2, h + 1.0 / 3.0)
        g = self._hue_to_rgb(m1, m2, h)
        b = self._hue_to_rgb(m1, m2, h - 1.0 / 3.0)

        # Apply gamma correction
        r **= self.gamma
        g **= self.gamma
        b **= self.gamma

        return (r, g, b)

    def _hue_to_rgb(self, m1, m2, h):
        if h < 0.0:
            h += 1.0
        elif h > 1.0:
            h -= 1.0
        if h * 6 < 1.0:
            return m1 + (m2 - m1) * h * 6.0
        elif h * 2 < 1.0:
            return m2
        elif h * 3 < 2.0:
            return m1 + (m2 - m1) * (2.0 / 3.0 - h) * 6.0
        else:
            return m1


TEMPERATURE_COLORMAP = Theme(
    mincolor=(2.0 / 3.0, 0.80, 0.25),  # dark blue
    maxcolor=(0.0, 1.0, 0.5),  # satured red
    gamma=1.0)

PINK_COLORMAP = Theme(
    mincolor=(0.0, 1.0, 0.90),  # pink
    maxcolor=(0.0, 1.0, 0.5),  # satured red
)

GRAY_COLORMAP = Theme(
    mincolor=(0.0, 0.0, 0.85),  # light gray
    maxcolor=(0.0, 0.0, 0.0),  # black
)

BW_COLORMAP = Theme(
    minfontsize=8.0,
    maxfontsize=24.0,
    mincolor=(0.0, 0.0, 0.0),  # black
    maxcolor=(0.0, 0.0, 0.0),  # black
    minpenwidth=0.1,
    maxpenwidth=8.0,
)

PRINT_COLORMAP = Theme(
    minfontsize=18.0,
    maxfontsize=30.0,
    fontcolor="black",
    nodestyle="solid",
    mincolor=(0.0, 0.0, 0.0),  # black
    maxcolor=(0.0, 0.0, 0.0),  # black
    minpenwidth=0.1,
    maxpenwidth=8.0,
)

themes = {
    "color": TEMPERATURE_COLORMAP,
    "pink": PINK_COLORMAP,
    "gray": GRAY_COLORMAP,
    "bw": BW_COLORMAP,
    "print": PRINT_COLORMAP,
}


def sorted_iteritems(d):
    # Used mostly for result reproducibility (while testing.)
    keys = compat_keys(d)
    keys.sort()
    for key in keys:
        value = d[key]
        yield key, value


class DotWriter:
    """Writer for the DOT language.

    See also:
    - "The DOT Language" specification
      http://www.graphviz.org/doc/info/lang.html
    """

    strip = False
    wrap = False

    def __init__(self, fp):
        self.fp = fp

    def wrap_function_name(self, name):
        """Split the function name on multiple lines."""

        if len(name) > 32:
            ratio = 2.0 / 3.0
            height = max(int(len(name) / (1.0 - ratio) + 0.5), 1)
            width = max(len(name) / height, 32)
            # TODO: break lines in symbols
            name = textwrap.fill(name, width, break_long_words=False)

        # Take away spaces
        name = name.replace(", ", ",")
        name = name.replace("> >", ">>")
        name = name.replace("> >", ">>")  # catch consecutive

        return name

    show_function_events = [TOTAL_TIME_RATIO, TIME_RATIO]
    show_edge_events = [FILE_LINE, CALLS]

    def graph(self, profile, theme):
        self.begin_graph()

        fontname = theme.graph_fontname()
        fontcolor = theme.graph_fontcolor()
        nodestyle = theme.node_style()

        self.attr('graph', fontname=fontname, ranksep=0.25, nodesep=0.125)
        self.attr(
            'node',
            fontname=fontname,
            shape="box",
            style=nodestyle,
            fontcolor=fontcolor,
            width=0,
            height=0)
        self.attr('edge', fontname=fontname)

        for _, function in sorted_iteritems(profile.functions):
            labels = []
            if function.process is not None:
                labels.append(function.process)
            if function.module is not None:
                labels.append(function.module)

            if self.strip:
                function_name = function.stripped_name()
            else:
                function_name = function.name

            # dot can't parse quoted strings longer than YY_BUF_SIZE, which
            # defaults to 16K. But some annotated C++ functions (e.g., boost,
            # https://github.com/jrfonseca/gprof2dot/issues/30) can exceed that
            MAX_FUNCTION_NAME = 4096
            if len(function_name) >= MAX_FUNCTION_NAME:
                sys.stderr.write(
                    'warning: truncating function name with %u chars (%s)\n' %
                    (len(function_name), function_name[:32] + '...'))
                function_name = function_name[:MAX_FUNCTION_NAME - 1] + unichr(0x2026)

            if self.wrap:
                function_name = self.wrap_function_name(function_name)
            labels.append(function_name)

            for event in self.show_function_events:
                if event in function.events:
                    label = event.format(function[event])
                    labels.append(label)
            if function.called is not None:
                labels.append("%u%s" % (function.called, MULTIPLICATION_SIGN))

            if function.weight is not None:
                weight = function.weight
            else:
                weight = 0.0

            label = '\n'.join(labels)
            self.node(
                function.id,
                label=label,
                color=self.color(theme.node_bgcolor(weight)),
                fontcolor=self.color(theme.node_fgcolor(weight)),
                fontsize="%.2f" % theme.node_fontsize(weight),
                tooltip=function.filename,
            )

            for _, call in sorted_iteritems(function.calls):
                callee = profile.functions[call.callee_id]

                labels = []
                for event in self.show_edge_events:
                    if event in call.events:
                        label = event.format(call[event])
                        labels.append(label)

                if call.weight is not None:
                    weight = call.weight
                elif callee.weight is not None:
                    weight = callee.weight
                else:
                    weight = 0.0

                label = '\n'.join(labels)

                self.edge(
                    function.id,
                    call.callee_id,
                    label=label,
                    color=self.color(theme.edge_color(weight)),
                    fontcolor=self.color(theme.edge_color(weight)),
                    fontsize="%.2f" % theme.edge_fontsize(weight),
                    penwidth="%.2f" % theme.edge_penwidth(weight),
                    labeldistance="%.2f" % theme.edge_penwidth(weight),
                    arrowsize="%.2f" % theme.edge_arrowsize(weight),
                )

        self.end_graph()

    def begin_graph(self):
        self.write('digraph {\n')

    def end_graph(self):
        self.write('}\n')

    def attr(self, what, **attrs):
        self.write("\t")
        self.write(what)
        self.attr_list(attrs)
        self.write(";\n")

    def node(self, node, **attrs):
        self.write("\t")
        self.id(node)
        self.attr_list(attrs)
        self.write(";\n")

    def edge(self, src, dst, **attrs):
        self.write("\t")
        self.id(src)
        self.write(" -> ")
        self.id(dst)
        self.attr_list(attrs)
        self.write(";\n")

    def attr_list(self, attrs):
        if not attrs:
            return
        self.write(' [')
        first = True
        for name, value in sorted_iteritems(attrs):
            if value is None:
                continue
            if first:
                first = False
            else:
                self.write(", ")
            self.id(name)
            self.write('=')
            self.id(value)
        self.write(']')

    def id(self, id):
        if isinstance(id, (int, float)):
            s = str(id)
        elif isinstance(id, basestring):
            if id.isalnum() and not id.startswith('0x'):
                s = id
            else:
                s = self.escape(id)
        else:
            raise TypeError
        self.write(s)

    def color(self, rgb):
        r, g, b = rgb

        def float2int(f):
            if f <= 0.0:
                return 0
            if f >= 1.0:
                return 255
            return int(255.0 * f + 0.5)

        return "#" + "".join(["%02x" % float2int(c) for c in (r, g, b)])

    def escape(self, s):
        if not PYTHON_3:
            s = s.encode('utf-8')
        s = s.replace('\\', r'\\')
        s = s.replace('\n', r'\n')
        s = s.replace('\t', r'\t')
        s = s.replace('"', r'\"')
        return '"' + s + '"'

    def write(self, s):
        self.fp.write(s)


########################################################################
# Main program


def naturalJoin(values):
    if len(values) >= 2:
        return ', '.join(values[:-1]) + ' or ' + values[-1]
    else:
        return ''.join(values)


def main(argv):
    """Main program."""

    formatNames = list(formats.keys())
    formatNames.sort()

    themeNames = list(themes.keys())
    themeNames.sort()

    optparser = optparse.OptionParser(usage="\n\t%prog [options] [file] ...")
    optparser.add_option(
        '-o',
        '--output',
        metavar='FILE',
        type="string",
        dest="output",
        help="output filename [stdout]")
    optparser.add_option(
        '-z',
        '--root',
        type="string",
        dest="root",
        default="main",
        help=
        "prune call graph to show only descendants of specified root function")
    optparser.add_option(
        '--outfmt',
        type="choice",
        choices=('dot', 'dump'),
        dest="outfmt",
        default='dot',
        help="preferred output format [default: %default]")
    optparser.add_option(
        '-f',
        '--format',
        type="choice",
        choices=formatNames,
        dest="format",
        default="gdbbt",
        help="profile format: %s [default: %%default]" %
        naturalJoin(formatNames))
    optparser.add_option(
        '-c',
        '--colormap',
        type="choice",
        choices=themeNames,
        dest="theme",
        default="color",
        help="color map: %s [default: %%default]" % naturalJoin(themeNames))
    optparser.add_option(
        '--depth',
        type="int",
        dest="depth",
        default=-1,
        help=
        "prune call graph to show only descendants or ancestors until specified depth"
    )

    #options=Struct()
    #options.node_thres=0.5
    (options, args) = optparser.parse_args(argv[1:])

    if len(args) > 1 and options.format != 'pstats':
        optparser.error('incorrect number of arguments')

    try:
        theme = themes[options.theme]
    except KeyError:
        optparser.error('invalid colormap \'%s\'' % options.theme)

    try:
        Format = formats[options.format]
    except KeyError:
        optparser.error('invalid format \'%s\'' % options.format)

    if Format.stdinInput:
        if not args:
            fp = sys.stdin
        elif PYTHON_3:
            fp = open(args[0], 'rt', encoding='UTF-8')
        else:
            fp = open(args[0], 'rt')
        parser = Format(fp)
    else:
        if len(args) != 1:
            optparser.error('exactly one file must be specified for %s input' %
                            options.format)
        parser = Format(args[0])

    profile = parser.parse()

    if options.output is None:
        if PYTHON_3:
            output = open(
                sys.stdout.fileno(),
                mode='wt',
                encoding='UTF-8',
                closefd=False)
        else:
            output = sys.stdout
    else:
        if PYTHON_3:
            output = open(options.output, 'wt', encoding='UTF-8')
        else:
            output = open(options.output, 'wt')

    if options.outfmt == "dump":
        #profile.dump()
        parser.dump(output)
        return

    dot = DotWriter(output)
    #if options.show_samples:
    #    dot.show_function_events.append(SAMPLES)

    rootIds = profile.getFunctionIds(options.root)
    if not rootIds:
        sys.stderr.write(
            'root node ' + options.root +
            ' not found (might already be pruned : try -e0 -n0 flags)\n')
        sys.exit(1)
    profile.prune_root(rootIds, options.depth)

    dot.graph(profile, theme)


if __name__ == '__main__':
    from sys import argv
    main(argv)

#./gdbcalltree.py core_output.txt | dot -o a.eps -Teps

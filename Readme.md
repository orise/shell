# Shell配置

- 历史命令自动搜索配置.inputrc，将找个文件拷贝到$HOME，重新登录shell即生效，输入历史命令前缀，再上下键搜索

- bin/create_module_file.sh，自动根据软件安装路径，构建其modulefile文件

- bin/modfile.sh 递归修改文件夹权限的脚本

  

# Slurm脚本

- sinfo2 Slurm增强sinfo的版本，可以查看当前账号可用队列等信息
- snode 提交作业并登录到计算节点上的脚本



# VS Code配置

提供VS Code配置方案

- Code源代码搜索设置，参考mtb1中的c_cpp_properties.json文件

- Code源代码调试设置，参考geant4中的tasks.json和settings.json文件



# VIM配置

在vimrc目录提供一种丰富设置的vim配置方案，拷贝到本地即使用

- install

```bash
cd vimrc/.vim/
tar xzvf plugged.tar.gz
cd ..
cp -r .vimrc .vim ~/
```

- install requirements

```bash
sudo apt install universal-ctags
sudo apt install cscope

#conda activate xxx to use python
```

# 关于集群访问FAQ

## 1.Windows操作系统推荐什么SSH终端

在Windows操作系统上**推荐SSH终端软件为mobaxterm** （`https://mobaxterm.mobatek.net`），它即使是免费版本，功能也超过当前很多同类软件，可以看着是Xshell、Xftp、XManager、Putty、WinSCP等软件的集大成者。除了优秀的Shell Terminal功能，还内置SCP文件浏览器，内置X Window服务端，甚至支持WSL的连接；针对SSH还提供Key Generator、SSHTunnel等功能模块，使用起来非常方便。

如果登录集群目的是编写代码，那么**推荐Visual Studio Code软件**，需要安装Remote SSH和WSL两个插件，之后在左小角会显示当前远程服务器连接状态，点击可以操作远程连接。使用以上两个插件可以实现像本地开发一样，浏览远程文件目录和编辑相应的代码文件，同时可以**打开Terminal**（默认自动连接SSH）进行Shell操作。



## 2.无法访问外网HTTP服务

**问题描述**

- 集群为了安全考虑（如东方超算系统），禁用了登录用户访问外网的权限。用户只能通过网络连入集群，但不可以从集群网络连出。外网访问被禁用，导致wget、pip、git、conda等网络在线访问工具不可用，给集群用户带来了极大的不方便。
- 有些软件资源需要翻qiang访问，即使集群用户可访问国内网络，也无法下载或者缓慢下载国外有些软件资源，如github.com经常无法访问。

**解决方案**

wget、pip、git、conda等工具的网络访问大都是HTTP服务，因此可以通过HTTP代理方式实现外网连接。由于集群普遍支持SSH登录访问，因此使用SSH内置的端口转发功能是HTTP代理搭建首选工具。

服务器集群一般基于Linux操作系统，通过设置http_proxy和https_proxy环境变量，就能得像wget、conda install和pip install等命令能够实现基于HTTP网络代理下载或安装新软件。

1. *启动squid服务*

Linux系统下提供squid代理服务，用户可以在某个已联网的Linux系统下安装和开启squid服务。Ubuntu Linux下使用命令

```
sudo apt update
sudo apt install squid
```

依据需要修改配置文件

```
sudo vi /etc/squid/squid.conf
```

服务的启动和重启

```
sudo service squid start
sudo service squid stop
sudo service squid restart
```

默认只允许localhost上，使用localhost:3218代理地址。在使用SSH端口转发时这已经足够。

2. *集群端访问配置*

以下以东方超算系统为例说明HTTP端口转发。东方超算系统登录IP为60.245.128.10，SSH服务端口为65010。在已安装squid (端口3128)服务的Linux系统上，执行一下SSH命令

```
ssh-keygen -f "/root/.ssh/known_hosts" -R "[60.245.128.10]:65010"
ssh -p 65010 -fNgv -R 9080:localhost:3128 yourcount@60.245.128.10
```

第一条命令是删除东方超算系统登录记录，因为东方超算系统登录是动态分配登录节点的，即有多个登录节点，每次登录可能会分配到不同的登录节点，主机名如login05~login10等。每次登录不同的节点会导致know_hosts和已有节点记录的冲突问题。这里通过ssh-keygen提前删除已有记录，就不会和新的记录冲突。

第二条命令就是用你账户yourcount（替换为实际用的账号名）登录东方超算系统，IP为60.245.128.10，通过-p指定连接SSH的端口。-R 9080:localhost:3128 选项是将本机（localhost，squid服务所在主机）的3128端口（对应squid服务）远处映射到60.245.128.10的9080端口。即可**实现在东方超算系统上访问9080端口，就等效于访问开启squid的那台主机的3128端口**。这里的9080端口可以自定义，但不能小于1024，也不要和常见的服务端口重复。

在执行第二条命令之后，会在东方超算系统上开启9080端口，但我们需要确定在东方超算系统的哪个分配的登录节点上开启了9080端口，这个需要我们轮询查看。

接下来正常登录东方超算系统，可以不在squid服务主机上发起连接。在Linux客户端用以下命令登录

```
ssh -p 65010 yourcount@60.245.128.10
```

如果在Windows系统下，推荐使用mobaxterm （`https://mobaxterm.mobatek.net`）SSH终端连接东方超算系统。

登录后（二次登录）所在的login主机，不一定和squid端口映射所在的主机是同一台。使用netstat命令查找确定哪台登录节点开启了9080端口，如

```
[yourcount@login10 ~]$ netstat -ln|grep 9080
tcp        0      0 127.0.0.1:9080          0.0.0.0:*               LISTEN
```

如果没有发现当前登录节点9080端口被打开，那么可能就在其他登录节点上，使用以下命令查找其他节点

```
for i in `seq -f "%02.0f" 1 10` ; do ssh login$i netstat -ln|grep 9080 ; done
```

**这里假设**查询到login10登录节点上存在SSH转发的squid 端口，之后SSH登录到login10节点，并设置HTTP代理环境变量。

```
[yourcount@login10 ~]$ export http_proxy=http://localhost:9080
[yourcount@login10 ~]$ export https_proxy=http://localhost:9080
```

注意https_proxy也是使用`http://`访问，不是`https://`。通过以上的设置，就可以在login10节点上，使用wget、git、pip、conda之类工具通过HTTP代理实现网络访问。因为只在login10节点上配置了HTTP代理，在其他登录节点无法使用代理。**即只有在squid端口转发到的登录节点上才可以使用HTTP代理服务**。

## 3.如何快捷登录集群中某个计算节点

高性能计算集群中通常包含**登录节点**和**计算节点**，登录节点数量有时不止一台，而计算节点甚至更多成百上千台。登录节点和计算节点的功能定位不同，因此两者的硬件配置也有差异。登录节点承担多用户登录和软件编译等任务，计算节点专注于承接用户作业计算任务，不过高性能计算集群中登录节点和计算节点往往存在共享的存储路径，方便应用程序在登录节点编译和在计算节点运行。对普通用户来说，更关心的是计算节点性能，因此有时需要了解计算节点的配置和性能，直接登录计算节点查看是最直接的方式。但是，在Slurm集群作业管理系统中，默认是无法SSH直接登录计算节点的，如何实现快捷登录计算节点？

解决方案上有两条途径：

1. 提交一个包含sleep命令的作业，在指定的时间内，可以SSH登录这个sleep作业所在的计算节点。缺点是sleep时长难以挑选得适合。
2. 使用Slurm提供的salloc命令分配一个计算节点，再用SSH登录这个计算节点。缺点是所分配的计算节点一直存在，不会自动退出。

这里基于第2个方案进行改造，**实现自动分配计算节点，并自动SSH登录到该计算节点，且操作完成之后自动退出**。首先需要新建一个可执行脚本，命名为[ssh_node](/ssh_node) 内容如下。需要修改其中的`-p normal`等选项，使得适配个人所用集群的环境，这里以东方超算系统环境为例。

```bash
#!/bin/bash
#ssh_node
# alloc one node and ssh it
#ssh_node 2
# alloc two nodes and ssh the first one
#ssh_node L
# ssh the first node within $SLURM_JOB_NODELIST

if [ "$1" = "L" ] ; then
  if [ -z "$SLURM_JOB_NODELIST" ] ; then
    echo "ssh error without \$SLURM_JOB_NODELIST"
  else
    set -x
    #sinfo
    squeue
    set +x
    s_node=$(scontrol show hostnames $SLURM_JOB_NODELIST |head -n 1)
    ssh $s_node
  fi
else
  if [ $# -gt 0 ]; then
   N=$1
  else
   N=1
  fi
  #salloc -p normal -w h02r1n02,i11r2n14 -N 1 --exclusive $0 L
  salloc -p normal --gres=dcu:4 -N $N --exclusive $0 L
 # salloc -p debug -N $N --exclusive $0 L
fi
```

这个ssh_node脚本通过salloc二次执行ssh_node脚本，并传入不同的参数选项来区分不同阶段的操作。ssh_node命令操作有三种方式：

1. `ssh_node`无任何输入参数时，默认分配1个计算节点并用ssh登录这个计算节点
2. `ssh_node N` 分配N个计算节点，并用ssh登录其中第一个计算节点
3. `ssh_node L` 依据`SLURM_JOB_NODELIST`环境变量获得计算节点的名称列表，用ssh登录第一个计算节点

`ssh_node L`命令是提供给ssh_node脚本自己使用的，用户只要使用第1和第2种操作方式。如果ssh_node中Slurm分区参数设置正确，执行ssh_node可实现直接调用ssh登录所分配的计算节点，退出时自动收回所分配的计算节点。

## 4.如何查看SLURM当前用户权限、队列和作业信息

SLURM 用户常关注可用队列信息、节点数、作业状态，需要一个命令输出这些简明实用的信息。可以针对SLURM提供的sinfo、scontrol和squeue命令输出信息进行了整合，提取一些简明实用的信息。新建sinfo2.sh脚本，其中设计sinfo2 Shell函数，以实现输出当前用户常关注的SLURM信息：

```bash
function sinfo2()
{
  sinfo|awk '{print $1,$2,$3,$4,$5; if( NR>1) {f[$5]+=$4;q[$1]+=$4} } END{ print ""; for (x in f) {print x,":",f[x];total+=f[x]}; print ""; for (x in q) {print x,":",q[x]}; print "\nTotal =", total }'
  echo
  scontrol show partition|awk "/PartitionName|MaxNodes/{print \$0}; /$USER/{print \"   AllowAccount\"}"
  echo
  scontrol show assoc_mgr  qos=user_${USER} flags=qos
  echo
  squeue
#  echo
#  sacct --format=jobid,start,end,Elapsed,Timelimit,CPUTimeRAW,NodeList,ReqCPUS,ReqMem,MaxRSS,MaxVMSize
}
```

使用`source sinfo2.sh`先导入Shell环境，就可以执行sinfo2 函数命令。也可以将sinfo2函数定义体（sinfo2.sh文件内容），直接拷贝到`$HOME/.bashrc`或`$HOME/.bash_profile`文件中，就可以使用。

" Detect Gaussian inputs
autocmd BufRead,BufNewFile *.g03 set filetype=gaussian
autocmd BufRead,BufNewFile *.g09 set filetype=gaussian
autocmd BufRead,BufNewFile *.gjf set filetype=gaussian
autocmd BufRead,BufNewFile *.com set filetype=gaussian

set nocompatible "不要vim模仿vi模式
" set paste
" set hlsearch "high light search result
filetype on
syntax on
set number
set showmatch
set ruler
set cursorline "highlight curline
set history=1000
set clipboard=unnamed "exclude:.*
let mapleader=","
set mouse=a
set selection=exclusive
set selectmode=mouse,key

" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')
" Make sure you use single quotes
" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
Plug 'junegunn/vim-easy-align'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'lilydjwg/colorizer'
Plug 'kshenoy/vim-signature'
Plug 'tpope/vim-fugitive'
Plug 'preservim/tagbar'
Plug 'derekwyatt/vim-fswitch'
Plug 'bioSyntax/bioSyntax-vim'
" Any valid git URL is allowed
" Plug 'https://github.com/junegunn/vim-github-dashboard.git'
" Multiple Plug commands can be written in a single line using | separators
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
" On-demand loading
" Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'preservim/nerdcommenter'
" Using a non-default branch
" Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }
" Using a tagged release; wildcard allowed (requires git 1.9.2 or above)
"Plug 'fatih/vim-go', { 'tag': '*' }
" Plugin options
"Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }
" Plugin outside ~/.vim/plugged with post-update hook
" Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
" Unmanaged plugin (manually installed and updated)
"Plug '~/my-prototype-plugin'
" Initialize plugin system
call plug#end()

set foldmethod=manual
set foldenable
set foldcolumn=2
" flod bracket
nmap z} /[{}]<CR>zfi{<CR>2k<CR> 
nmap z{ ?[{}]<CR>zfi{<CR>2k<CR> 

nnoremap wp :bp<CR>
nnoremap wn :bn<CR>
nnoremap wd :bd<CR>
nnoremap wf :NERDTreeToggle<CR>
" autocmd VimEnter * NERDTree

nmap wt :TagbarToggle<CR> " 将开启tagbar的快捷键设置
let g:tagbar_ctags_bin='/usr/bin/ctags' " 设置ctags所在路径
let g:tagbar_width=30 " 设置tagbar的宽度
autocmd BufReadPost *.h,*.hh,*.hpp,*.c,*.cpp,*.cc,*.cxx call tagbar#autoopen() " 在某些情况下自动打开tagbar

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)


nmap wh :vertical resize -3<CR>
nmap wj :resize -3<CR>
nmap wk :resize +3<CR>
nmap wl :vertical resize +3<CR>

map <C-S> :w<CR> "ctrl+s to save
map <C-A> gg<CR>v<CR>G "ctrl+a to select all

map <C-I> :%py3file /usr/share/vim/addons/syntax/clang-format.py<cr>
imap <C-I> <c-o>:%py3file /usr/share/vim/addons/syntax/clang-format.py<cr>
"Generate tags and cscope.out from FileList.txt (c, cpp, h, hpp)
nmap <C-@> :!find -name "*.c" -o -name "*.cpp" -o -name "*.cc" -o -name "*.h" -o -name "*.hh"  -o -name "*.hpp" > cscope.in.txt<CR>
                       \ :!ctags -L -< cscope.in.txt<CR>
                       \ :!cscope -bkq -i cscope.in.txt<CR>

if has("cscope")
    set csto=0
    set nocsverb
    " add any database in current directory
    if filereadable("cscope.out")
        cs add cscope.out
    endif
    set csverb
    "set cst  这两句会将cscope当作tag，当找不到时会卡住，因此注释掉
    "set cscopetag
endif

nmap <C-\>a :cs add cscope.out<cr>
nmap <C-\>s :cs find s <C-R>=expand("<cword>")<CR><CR>
nmap <C-\>g :cs find g <C-R>=expand("<cword>")<CR><CR>
nmap <C-\>c :cs find c <C-R>=expand("<cword>")<CR><CR>
nmap <C-\>t :cs find t <C-R>=expand("<cword>")<CR><CR>
nmap <C-\>e :cs find e <C-R>=expand("<cword>")<CR><CR>
nmap <C-\>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
nmap <C-\>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
nmap <C-\>d :cs find d <C-R>=expand("<cword>")<CR><CR>

"get calltree at current file and current line, export GDB_ARGS=exe-file [args]
nmap <C-\>b :!gdbcalltree.sh <C-R>=expand("%t").":".line(".")<CR><CR>
"File switch header
nmap <C-\>h :FSHere<CR>
nmap <C-\>l :FSLeft<CR>

set laststatus=2  "永远显示状态栏
let g:airline#extensions#tabline#enabled = 1 "显示窗口tab和buffer
let g:airline_theme='molokai'  " murmur配色不错
if !exists('g:airline_symbols')
 let g:airline_symbols = {}
endif
let g:airline_left_sep = '▶'
let g:airline_left_alt_sep = '❯'
let g:airline_right_sep = '◀'
let g:airline_right_alt_sep = '❮'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_detect_modified=1
let g:airline_detect_paste=1

" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1
" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'
" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1
" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1
" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1
" Enable NERDCommenterToggle to check all selected lines is commented or not
let g:NERDToggleCheckAllLines = 1

nnoremap <silent> <leader>c} V}:call nerdcommenter#Comment('x', 'toggle')<CR>
nnoremap <silent> <leader>c{ V{:call nerdcommenter#Comment('x', 'toggle')<CR>

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

autocmd FileType mdp.gromacs set omnifunc=mdpcomplete#Complete
let g:SignatureMap = {
        \ 'Leader'             :  "m",
        \ 'PlaceNextMark'      :  "m,",
        \ 'ToggleMarkAtLine'   :  "m.",
        \ 'PurgeMarksAtLine'   :  "m-",
        \ 'DeleteMark'         :  "dm",
        \ 'PurgeMarks'         :  "mda",
        \ 'PurgeMarkers'       :  "m<BS>",
        \ 'GotoNextLineAlpha'  :  "']",
        \ 'GotoPrevLineAlpha'  :  "'[",
        \ 'GotoNextSpotAlpha'  :  "`]",
        \ 'GotoPrevSpotAlpha'  :  "`[",
        \ 'GotoNextLineByPos'  :  "]'",
        \ 'GotoPrevLineByPos'  :  "['",
        \ 'GotoNextSpotByPos'  :  "mn",
        \ 'GotoPrevSpotByPos'  :  "mp",
        \ 'GotoNextMarker'     :  "[+",
        \ 'GotoPrevMarker'     :  "[-",
        \ 'GotoNextMarkerAny'  :  "]=",
        \ 'GotoPrevMarkerAny'  :  "[=",
        \ 'ListLocalMarks'     :  "ms",
        \ 'ListLocalMarkers'   :  "m?"
        \ }
